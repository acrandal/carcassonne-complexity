# Carcassonne Complexity


The initial goal of this project is to simulate enough of the board game Carcassonne as to determine a max score in the game.
The simulation will be using the 3rd edition basic box.


Overall, it's a hack of a program using a brute force algorithm.
We'll see if it's even tractable.


License Information:

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#" class="license-text"><a rel="cc:attributionURL" property="dct:title" href="https://gitlab.com/acrandal/carcassonne-complexity">Carcassonne Complexity</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="acrandal@gmail.com">Aaron S. Crandall, PhD</a> is licensed under <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" /><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" /><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1" /><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1" /></a></p>